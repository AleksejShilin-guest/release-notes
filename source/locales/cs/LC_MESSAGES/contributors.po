# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2023, Debian Documentation Team
# This file is distributed under the same license as the release-notes
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: release-notes 13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-15 18:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../contributors.rst:4
msgid "Contributors to the Release Notes"
msgstr ""

#: ../contributors.rst:6
msgid "Many people helped with the release notes, including, but not limited to"
msgstr ""

#: ../contributors.rst:8
msgid ":abbr:`Adam D. Barrat (various fixes in 2013)`,"
msgstr ""

#: ../contributors.rst:9
msgid ":abbr:`Adam Di Carlo (previous releases)`,"
msgstr ""

#: ../contributors.rst:10
msgid ":abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,"
msgstr ""

#: ../contributors.rst:11
msgid ":abbr:`Andrei Popescu (various contributions)`,"
msgstr ""

#: ../contributors.rst:12
msgid ":abbr:`Anne Bezemer (previous release)`,"
msgstr ""

#: ../contributors.rst:13
msgid ":abbr:`Bob Hilliard (previous release)`,"
msgstr ""

#: ../contributors.rst:14
msgid ":abbr:`Charles Plessy (description of GM965 issue)`,"
msgstr ""

#: ../contributors.rst:15
msgid ":abbr:`Christian Perrier bubulle (Lenny installation)`,"
msgstr ""

#: ../contributors.rst:16
msgid ":abbr:`Christoph Berg (PostgreSQL-specific issues)`,"
msgstr ""

#: ../contributors.rst:17
msgid ":abbr:`Daniel Baumann (Debian Live)`,"
msgstr ""

#: ../contributors.rst:18
msgid ":abbr:`David Prévot taffit (Wheezy release)`,"
msgstr ""

#: ../contributors.rst:19
msgid ":abbr:`Eddy Petrișor (various contributions)`,"
msgstr ""

#: ../contributors.rst:20
msgid ":abbr:`Emmanuel Kasper (backports)`,"
msgstr ""

#: ../contributors.rst:21
msgid ":abbr:`Esko Arajärvi (rework X11 upgrade)`,"
msgstr ""

#: ../contributors.rst:22
msgid ":abbr:`Frans Pop fjp (previous release Etch)`,"
msgstr ""

#: ../contributors.rst:23
msgid ":abbr:`Giovanni Rapagnani (innumerable contributions)`,"
msgstr ""

#: ../contributors.rst:24
msgid ":abbr:`Gordon Farquharson (ARM port issues)`,"
msgstr ""

#: ../contributors.rst:25
msgid ":abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,"
msgstr ""

#: ../contributors.rst:26
msgid ":abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,"
msgstr ""

#: ../contributors.rst:27
msgid ""
":abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze "
"release)`,"
msgstr ""

#: ../contributors.rst:28
msgid ":abbr:`Jens Seidel (German translation, innumerable contributions)`,"
msgstr ""

#: ../contributors.rst:29
msgid ":abbr:`Jonas Meurer (syslog issues)`,"
msgstr ""

#: ../contributors.rst:30
msgid ":abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,"
msgstr ""

#: ../contributors.rst:31
msgid ":abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,"
msgstr ""

#: ../contributors.rst:32
msgid ":abbr:`Josip Rodin (previous releases)`,"
msgstr ""

#: ../contributors.rst:33
msgid ":abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,"
msgstr ""

#: ../contributors.rst:34
msgid ":abbr:`Justin B Rye (English fixes)`,"
msgstr ""

#: ../contributors.rst:35
msgid ":abbr:`LaMont Jones (description of NFS issues)`,"
msgstr ""

#: ../contributors.rst:36
msgid ":abbr:`Luk Claes (editors motivation manager)`,"
msgstr ""

#: ../contributors.rst:37
msgid ":abbr:`Martin Michlmayr (ARM port issues)`,"
msgstr ""

#: ../contributors.rst:38
msgid ":abbr:`Michael Biebl (syslog issues)`,"
msgstr ""

#: ../contributors.rst:39
msgid ":abbr:`Moritz Mühlenhoff (various contributions)`,"
msgstr ""

#: ../contributors.rst:40
msgid ":abbr:`Niels Thykier nthykier (Jessie release)`,"
msgstr ""

#: ../contributors.rst:41
msgid ":abbr:`Noah Meyerhans (innumerable contributions)`,"
msgstr ""

#: ../contributors.rst:42
msgid ""
":abbr:`Noritada Kobayashi (Japanese translation (coordination), "
"innumerable contributions)`,"
msgstr ""

#: ../contributors.rst:43
msgid ":abbr:`Osamu Aoki (various contributions)`,"
msgstr ""

#: ../contributors.rst:44
msgid ":abbr:`Paul Gevers elbrus (buster release)`,"
msgstr ""

#: ../contributors.rst:45
msgid ":abbr:`Peter Green (kernel version note)`,"
msgstr ""

#: ../contributors.rst:46
msgid ":abbr:`Rob Bradford (Etch release)`,"
msgstr ""

#: ../contributors.rst:47
msgid ":abbr:`Samuel Thibault (description of d-i Braille support)`,"
msgstr ""

#: ../contributors.rst:48
msgid ":abbr:`Simon Bienlein (description of d-i Braille support)`,"
msgstr ""

#: ../contributors.rst:49
msgid ":abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,"
msgstr ""

#: ../contributors.rst:50
msgid ":abbr:`Stefan Fritsch (description of Apache issues)`,"
msgstr ""

#: ../contributors.rst:51
msgid ":abbr:`Steve Langasek (Etch release)`,"
msgstr ""

#: ../contributors.rst:52
msgid ":abbr:`Steve McIntyre (Debian CDs)`,"
msgstr ""

#: ../contributors.rst:53
msgid ":abbr:`Tobias Scherer (description of \"proposed-update\")`,"
msgstr ""

#: ../contributors.rst:54
msgid ""
":abbr:`victory victory-guest (markup fixes, contributed and contributing "
"since 2006)`,"
msgstr ""

#: ../contributors.rst:55
msgid ":abbr:`Vincent McIntyre (description of \"proposed-update\")`,"
msgstr ""

#: ../contributors.rst:56
msgid ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."
msgstr ""

#: ../contributors.rst:58
msgid ""
"This document has been translated into many languages. Many thanks to all"
" the translators!"
msgstr ""

#~ msgid ""
#~ ":abbr:`Adam D. Barrat (various fixes in"
#~ " 2013)`, :abbr:`Adam Di Carlo (previous "
#~ "releases)`, :abbr:`Andreas Barth aba (previous"
#~ " releases: 2005 - 2007)`, :abbr:`Andrei "
#~ "Popescu (various contributions)`, :abbr:`Anne "
#~ "Bezemer (previous release)`, :abbr:`Bob "
#~ "Hilliard (previous release)`, :abbr:`Charles "
#~ "Plessy (description of GM965 issue)`, "
#~ ":abbr:`Christian Perrier bubulle (Lenny "
#~ "installation)`, :abbr:`Christoph Berg "
#~ "(PostgreSQL-specific issues)`, :abbr:`Daniel "
#~ "Baumann (Debian Live)`, :abbr:`David Prévot"
#~ " taffit (Wheezy release)`, :abbr:`Eddy "
#~ "Petrișor (various contributions)`, :abbr:`Emmanuel"
#~ " Kasper (backports)`, :abbr:`Esko Arajärvi "
#~ "(rework X11 upgrade)`, :abbr:`Frans Pop "
#~ "fjp (previous release Etch)`, :abbr:`Giovanni"
#~ " Rapagnani (innumerable contributions)`, "
#~ ":abbr:`Gordon Farquharson (ARM port issues)`,"
#~ " :abbr:`Hideki Yamane henrich (contributed "
#~ "and contributing since 2006)`, :abbr:`Holger"
#~ " Wansing holgerw (contributed and "
#~ "contributing since 2009)`, :abbr:`Javier "
#~ "Fernández-Sanguino Peña jfs (Etch release,"
#~ " Squeeze release)`, :abbr:`Jens Seidel "
#~ "(German translation, innumerable contributions)`,"
#~ " :abbr:`Jonas Meurer (syslog issues)`, "
#~ ":abbr:`Jonathan Nieder jrnieder@gmail.com (Squeeze"
#~ " release, Wheezy release)`, :abbr:`Joost "
#~ "van Baal-Ilić joostvb (Wheezy release,"
#~ " Jessie release)`, :abbr:`Josip Rodin "
#~ "(previous releases)`, :abbr:`Julien Cristau "
#~ "jcristau (Squeeze release, Wheezy release)`,"
#~ " :abbr:`Justin B Rye (English fixes)`, "
#~ ":abbr:`LaMont Jones (description of NFS "
#~ "issues)`, :abbr:`Luk Claes (editors motivation"
#~ " manager)`, :abbr:`Martin Michlmayr (ARM "
#~ "port issues)`, :abbr:`Michael Biebl (syslog"
#~ " issues)`, :abbr:`Moritz Mühlenhoff (various "
#~ "contributions)`, :abbr:`Niels Thykier nthykier "
#~ "(Jessie release)`, :abbr:`Noah Meyerhans "
#~ "(innumerable contributions)`, :abbr:`Noritada "
#~ "Kobayashi (Japanese translation (coordination), "
#~ "innumerable contributions)`, :abbr:`Osamu Aoki "
#~ "(various contributions)`, :abbr:`Paul Gevers "
#~ "elbrus (buster release)`, :abbr:`Peter Green"
#~ " (kernel version note)`, :abbr:`Rob "
#~ "Bradford (Etch release)`, :abbr:`Samuel "
#~ "Thibault (description of d-i Braille "
#~ "support)`, :abbr:`Simon Bienlein (description "
#~ "of d-i Braille support)`, :abbr:`Simon "
#~ "Paillard spaillar-guest (innumerable "
#~ "contributions)`, :abbr:`Stefan Fritsch (description"
#~ " of Apache issues)`, :abbr:`Steve Langasek"
#~ " (Etch release)`, :abbr:`Steve McIntyre "
#~ "(Debian CDs)`, :abbr:`Tobias Scherer "
#~ "(description of \"proposed-update\")`, "
#~ ":abbr:`victory victory-guest victory.deb@gmail.com"
#~ " (markup fixes, contributed and "
#~ "contributing since 2006)`, :abbr:`Vincent "
#~ "McIntyre (description of \"proposed-"
#~ "update\")`, and :abbr:`W. Martin Borgert "
#~ "(editing Lenny release, switch to "
#~ "DocBook XML)`."
#~ msgstr ""

#~ msgid ""
#~ ":abbr:`Jonathan Nieder jrnieder@gmail.com (Squeeze"
#~ " release, Wheezy release)`,"
#~ msgstr ""

#~ msgid ""
#~ ":abbr:`victory victory-guest victory.deb@gmail.com"
#~ " (markup fixes, contributed and "
#~ "contributing since 2006)`,"
#~ msgstr ""

