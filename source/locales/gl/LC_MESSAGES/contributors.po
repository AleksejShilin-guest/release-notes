# Translation of release-notes.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
# Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno
# (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl), nese
# orde de preferencia
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2024-01-15 18:03+0100\n"
"PO-Revision-Date: 2023-06-12 09:34+0200\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language: gl\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../contributors.rst:4
msgid "Contributors to the Release Notes"
msgstr "Contribuíntes das Notas da Versión"

#: ../contributors.rst:6
msgid "Many people helped with the release notes, including, but not limited to"
msgstr "Moita xente axudou coas notas da versión, incluíndo entre eles"

#: ../contributors.rst:8
msgid ":abbr:`Adam D. Barrat (various fixes in 2013)`,"
msgstr ":abbr:`Adam D. Barrat (arranxos varios en 2013)`,"

#: ../contributors.rst:9
msgid ":abbr:`Adam Di Carlo (previous releases)`,"
msgstr ":abbr:`Adam Di Carlo (versións previas)`,"

#: ../contributors.rst:10
msgid ":abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,"
msgstr ":abbr:`Andreas Barth aba (versións previas: 2005 - 2007)`,"

#: ../contributors.rst:11
msgid ":abbr:`Andrei Popescu (various contributions)`,"
msgstr ":abbr:`Andrei Popescu (contribucións varias)`,"

#: ../contributors.rst:12
msgid ":abbr:`Anne Bezemer (previous release)`,"
msgstr ":abbr:`Anne Bezemer (versión previa)`,"

#: ../contributors.rst:13
msgid ":abbr:`Bob Hilliard (previous release)`,"
msgstr ":abbr:`Bob Hilliard (versión previa)`,"

#: ../contributors.rst:14
msgid ":abbr:`Charles Plessy (description of GM965 issue)`,"
msgstr ":abbr:`Charles Plessy (descrición do problema GM965)`,"

#: ../contributors.rst:15
msgid ":abbr:`Christian Perrier bubulle (Lenny installation)`,"
msgstr ":abbr:`Christian Perrier bubulle (instalación do Lenny)`,"

#: ../contributors.rst:16
msgid ":abbr:`Christoph Berg (PostgreSQL-specific issues)`,"
msgstr ":abbr:`Christoph Berg (problemas propios do PostgreSQL)`,"

#: ../contributors.rst:17
msgid ":abbr:`Daniel Baumann (Debian Live)`,"
msgstr ":abbr:`Daniel Baumann (Debian Live)`,"

#: ../contributors.rst:18
msgid ":abbr:`David Prévot taffit (Wheezy release)`,"
msgstr ":abbr:`David Prévot taffit (versión Wheezy)`,"

#: ../contributors.rst:19
msgid ":abbr:`Eddy Petrișor (various contributions)`,"
msgstr ":abbr:`Eddy Petrișor (contribucións varias)`,"

#: ../contributors.rst:20
msgid ":abbr:`Emmanuel Kasper (backports)`,"
msgstr ":abbr:`Emmanuel Kasper (parches de mantemento)`,"

#: ../contributors.rst:21
msgid ":abbr:`Esko Arajärvi (rework X11 upgrade)`,"
msgstr ":abbr:`Esko Arajärvi (refixo a actualización de X11)`,"

#: ../contributors.rst:22
msgid ":abbr:`Frans Pop fjp (previous release Etch)`,"
msgstr ":abbr:`Frans Pop fjp (versión previa Etch)`,"

#: ../contributors.rst:23
msgid ":abbr:`Giovanni Rapagnani (innumerable contributions)`,"
msgstr ":abbr:`Giovanni Rapagnani (incontables contribucións)`,"

#: ../contributors.rst:24
msgid ":abbr:`Gordon Farquharson (ARM port issues)`,"
msgstr ":abbr:`Gordon Farquharson (problemas coa adaptación a ARM)`,"

#: ../contributors.rst:25
msgid ":abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,"
msgstr ":abbr:`Hideki Yamane henrich (contribuía e contribúe dende 2006)`,"

#: ../contributors.rst:26
msgid ":abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,"
msgstr ":abbr:`Holger Wansing holgerw (contribuía e contribúe dende 2009)`,"

#: ../contributors.rst:27
msgid ""
":abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze "
"release)`,"
msgstr ""
":abbr:`Javier Fernández-Sanguino Peña jfs (versión Etch, versión "
"Squeeze)`,"

#: ../contributors.rst:28
msgid ":abbr:`Jens Seidel (German translation, innumerable contributions)`,"
msgstr ":abbr:`Jens Seidel (Tradución ao alemán, innumerables contribucións)`,"

#: ../contributors.rst:29
msgid ":abbr:`Jonas Meurer (syslog issues)`,"
msgstr ":abbr:`Jonas Meurer (problemas co rexistro do sistema (syslog)`,"

#: ../contributors.rst:30
msgid ":abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,"
msgstr ""
":abbr:`Jonathan Nieder (versión Squeeze, versión Wheezy)`,"

#: ../contributors.rst:31
msgid ":abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,"
msgstr ":abbr:`Joost van Baal-Ilić joostvb (versión Wheezy, versión Jessie)`,"

#: ../contributors.rst:32
msgid ":abbr:`Josip Rodin (previous releases)`,"
msgstr ":abbr:`Josip Rodin (versións previas)`,"

#: ../contributors.rst:33
msgid ":abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,"
msgstr ":abbr:`Julien Cristau jcristau (versión Squeeze, versión Wheezy)`,"

#: ../contributors.rst:34
msgid ":abbr:`Justin B Rye (English fixes)`,"
msgstr ":abbr:`Justin B Rye (Arranxos no texto en inglés)`,"

#: ../contributors.rst:35
msgid ":abbr:`LaMont Jones (description of NFS issues)`,"
msgstr ":abbr:`LaMont Jones (descrición dos problemas de NFS)`,"

#: ../contributors.rst:36
msgid ":abbr:`Luk Claes (editors motivation manager)`,"
msgstr ":abbr:`Luk Claes (encargado da motivación dos editores)`,"

#: ../contributors.rst:37
msgid ":abbr:`Martin Michlmayr (ARM port issues)`,"
msgstr ":abbr:`Martin Michlmayr (problemas coa adaptación a ARM)`,"

#: ../contributors.rst:38
msgid ":abbr:`Michael Biebl (syslog issues)`,"
msgstr ":abbr:`Michael Biebl (problemas co rexistro do sistema (syslog)`,"

#: ../contributors.rst:39
msgid ":abbr:`Moritz Mühlenhoff (various contributions)`,"
msgstr ":abbr:`Moritz Mühlenhoff (contribucións varias)`,"

#: ../contributors.rst:40
msgid ":abbr:`Niels Thykier nthykier (Jessie release)`,"
msgstr ":abbr:`Niels Thykier nthykier (versión Jessie)`,"

#: ../contributors.rst:41
msgid ":abbr:`Noah Meyerhans (innumerable contributions)`,"
msgstr ":abbr:`Noah Meyerhans (incontables contribucións)`,"

#: ../contributors.rst:42
msgid ""
":abbr:`Noritada Kobayashi (Japanese translation (coordination), "
"innumerable contributions)`,"
msgstr ""
":abbr:`Noritada Kobayashi (Tradución ao xaponés (coordinación), "
"incontables contribucións)`,"

#: ../contributors.rst:43
msgid ":abbr:`Osamu Aoki (various contributions)`,"
msgstr ":abbr:`Osamu Aoki (contribucións varias)`,"

#: ../contributors.rst:44
msgid ":abbr:`Paul Gevers elbrus (buster release)`,"
msgstr ":abbr:`Paul Gevers elbrus (versión Buster)`,"

#: ../contributors.rst:45
msgid ":abbr:`Peter Green (kernel version note)`,"
msgstr ":abbr:`Peter Green (notas da versión do núcleo)`,"

#: ../contributors.rst:46
msgid ":abbr:`Rob Bradford (Etch release)`,"
msgstr ":abbr:`Rob Bradford (versión Etch)`,"

#: ../contributors.rst:47
msgid ":abbr:`Samuel Thibault (description of d-i Braille support)`,"
msgstr ":abbr:`Samuel Thibault (descrición da compatibilidade do Braille no d-i)`,"

#: ../contributors.rst:48
msgid ":abbr:`Simon Bienlein (description of d-i Braille support)`,"
msgstr ":abbr:`Simon Bienlein (descrición da compatibilidade do Braille no d-i)`,"

#: ../contributors.rst:49
msgid ":abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,"
msgstr ":abbr:`Simon Paillard spaillar-guest (incontables contribucións)`,"

#: ../contributors.rst:50
msgid ":abbr:`Stefan Fritsch (description of Apache issues)`,"
msgstr ":abbr:`Stefan Fritsch (descrición dos problemas de Apache)`,"

#: ../contributors.rst:51
msgid ":abbr:`Steve Langasek (Etch release)`,"
msgstr ":abbr:`Steve Langasek (versión Etch)`,"

#: ../contributors.rst:52
msgid ":abbr:`Steve McIntyre (Debian CDs)`,"
msgstr ":abbr:`Steve McIntyre (Discos de Debian)`,"

#: ../contributors.rst:53
msgid ":abbr:`Tobias Scherer (description of \"proposed-update\")`,"
msgstr ":abbr:`Tobias Scherer (descrición de \"proposed-update\")`,"

#: ../contributors.rst:54
msgid ""
":abbr:`victory victory-guest (markup fixes, contributed and contributing "
"since 2006)`,"
msgstr ""
":abbr:`victory victory-guest (arranxos no estilo, contribuía e contribúe dende 2006)`,"

#: ../contributors.rst:55
msgid ":abbr:`Vincent McIntyre (description of \"proposed-update\")`,"
msgstr ":abbr:`Vincent McIntyre (descrición de \"proposed-update\")`,"

#: ../contributors.rst:56
msgid ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."
msgstr ":abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`."

#: ../contributors.rst:58
msgid ""
"This document has been translated into many languages. Many thanks to all"
" the translators!"
msgstr ""
"Este documento foi traducido a moitos idiomas.  Estámoslle moi "
"agradecidos aos tradutores! Traducido ao galego por: :abbr:`Pablo "
"\"parodper\" (Tradución inicial)`"

