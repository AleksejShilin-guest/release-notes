# Swedish translation of release-notes for Debian
# Copyright (C) 2006, 2007, 2008, 2009, 2011, 2013 2017, 2019, 2021 Free
# Software Foundation, Inc.
#
# Martin Bagge <brother@bsnet.se>, 2009, 2011, 2013, 2017, 2019, 2021.
# Martin Ågren <martin.agren@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: release-notes old-stuff.po\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2024-04-22 20:43+0200\n"
"PO-Revision-Date: 2021-07-27 00:25+0200\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language: sv\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../old-stuff.rst:4
msgid "Managing your |OLDRELEASENAME| system before the upgrade"
msgstr "Hantera ditt |OLDRELEASENAME|-system före uppgraderingen"

#: ../old-stuff.rst:6
msgid ""
"This appendix contains information on how to make sure you can install or"
" upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|."
msgstr ""
"Denna bilaga innehåller information om hur du kontrollerar att du kan "
"installera eller uppgradera paket från |OLDRELEASENAME| innan du "
"uppgraderar till |RELEASENAME|."

#: ../old-stuff.rst:12
msgid "Upgrading your |OLDRELEASENAME| system"
msgstr "Uppgradering av ditt |OLDRELEASENAME|-system"

#: ../old-stuff.rst:14
#, fuzzy
msgid ""
"Basically this is no different from any other upgrade of |OLDRELEASENAME|"
" you've been doing. The only difference is that you first need to make "
"sure your package list still contains references to |OLDRELEASENAME| as "
"explained in `Checking your APT source-list files <#old-sources>`__."
msgstr ""
"Det är inga grundläggande skillnader mot någon annan uppgradering av "
"|OLDRELEASENAME| som du gjort. Den enda skillnaden är att du först "
"behöver se till att din paketlista fortfarande innehåller paket från "
"|OLDRELEASENAME|, vilket förklaras i `Checking your APT source-list files"
" <#old-sources>`__."

#: ../old-stuff.rst:19
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically "
"be upgraded to the latest |OLDRELEASENAME| point release."
msgstr ""
"Om du uppgraderar ditt system med en Debianspegel kommer den automatiskt "
"att uppgraderas till den senaste punktutgåvan av |OLDRELEASENAME|."

#: ../old-stuff.rst:25
msgid "Checking your APT source-list files"
msgstr "Kontrollera dina APT sources.list-filer"

#: ../old-stuff.rst:27
msgid ""
"If any of the lines in your APT source-list files (see :url-man-"
"stable:`sources.list(5)`) contain references to \"stable\", this is "
"effectively pointing to |RELEASENAME| already. This might not be what you"
" want if you are not yet ready for the upgrade. If you have already run "
"``apt update``, you can still get back without problems by following the "
"procedure below."
msgstr ""
"Om någon av raderna i dina APT source.list-filer (se även :url-man-"
"stable:`sources.list(5)`) refererar till \"stable\" innebär detta att du "
"redan pekar ut |RELEASENAME|. Det kanske inte är vad du vill göra om du "
"inte är redo för uppgraderingen än.  Om du redan har kört ``apt update``,"
" kan du fortfarande komma tillbaka utan problem om du följer nedanstående"
" procedur."

#: ../old-stuff.rst:34
msgid ""
"If you have also already installed packages from |RELEASENAME|, there "
"probably is not much point in installing packages from |OLDRELEASENAME| "
"anymore. In that case you will have to decide for yourself whether you "
"want to continue or not. It is possible to downgrade packages, but that "
"is not covered here."
msgstr ""
"Om du även har installerat paket från |RELEASENAME|, är det antagligen "
"inte så stor mening att installera paket från |OLDRELEASENAME| längre. I "
"det fallet måste du bestämma dig för om du vill fortsätta eller inte. Det"
" är möjligt att nedgradera paket, men det beskrivs inte här."

#: ../old-stuff.rst:40
#, fuzzy
msgid ""
"As root, open the relevant APT source-list file (such as "
"``/etc/apt/sources.list``) with your favorite editor, and check all lines"
" beginning with"
msgstr ""
"Redigera relevanta APT source-list filer, exempelvis "
"``/etc/apt/sources.list`` (som root) och kontrollera alla rader som "
"börjar med "

#: ../old-stuff.rst:44
msgid "``deb http:``"
msgstr ""

#: ../old-stuff.rst:45
msgid "``deb https:``"
msgstr ""

#: ../old-stuff.rst:46
msgid "``deb tor+http:``"
msgstr ""

#: ../old-stuff.rst:47
msgid "``deb tor+https:``"
msgstr ""

#: ../old-stuff.rst:48
msgid "``URIs: http:``"
msgstr ""

#: ../old-stuff.rst:49
msgid "``URIs: https:``"
msgstr ""

#: ../old-stuff.rst:50
msgid "``URIs: tor+http:``"
msgstr ""

#: ../old-stuff.rst:51
msgid "``URIs: tor+https:``"
msgstr ""

#: ../old-stuff.rst:53
#, fuzzy
msgid ""
"for a reference to \"stable\". If you find any, change \"stable\" to "
"\"|OLDRELEASENAME|\"."
msgstr ""
"efter en referens till \"stable\".  Om du hittar någon, ändra \"stable\" "
"till \"|OLDRELEASENAME|\"."

#: ../old-stuff.rst:55
msgid ""
"If you have any lines starting with ``deb file:`` or ``URIs: file:``, you"
" will have to check for yourself if the location they refer to contains a"
" |OLDRELEASENAME| or |RELEASENAME| archive."
msgstr ""
"Om du har vissa rader som börjar med ``deb file:`` eller ``URIs: file:`` "
"måste du själv kontrollera om platsen som de refererar till innehåller "
"ett arkiv för |OLDRELEASENAME| eller |RELEASENAME|."

#: ../old-stuff.rst:61
msgid ""
"Do not change any lines that begin with ``deb cdrom:`` or ``URIs: "
"cdrom:``. Doing so would invalidate the line and you would have to run "
"``apt-cdrom`` again. Do not be alarmed if a ``cdrom:`` source line refers"
" to \"unstable\". Although confusing, this is normal."
msgstr ""
"Ändra inte några rader som börjar med ``deb cdrom:`` eller ``URIs: "
"cdrom:``.  Om du gör det så ogiltigförklaras raden och du måste köra "
"``apt-cdrom`` igen.  Bli inte rädd om en ``cdrom``-källrad refererar till"
" \"unstable\".  Även om det är förvirrande så är det normalt."

#: ../old-stuff.rst:67
msgid "If you've made any changes, save the file and execute"
msgstr "Om du har gjort några ändringar, spara filen och kör"

#: ../old-stuff.rst:73
msgid "to refresh the package list."
msgstr "för att uppdatera paketlistan."

#: ../old-stuff.rst:78
msgid "Performing the upgrade to latest |OLDRELEASENAME| release"
msgstr ""

#: ../old-stuff.rst:80
msgid ""
"To upgrade all packages to the state of the latest point release for "
"|OLDRELEASENAME|, do"
msgstr ""

#: ../old-stuff.rst:90
msgid "Removing obsolete configuration files"
msgstr "Ta bort oanvända inställningsfiler"

#: ../old-stuff.rst:92
msgid ""
"Before upgrading your system to |RELEASENAME|, it is recommended to "
"remove old configuration files (such as ``*.dpkg-{new,old}`` files under "
"``/etc``) from the system."
msgstr ""
"Innan uppgradering av systemet till |RELEASENAME| rekomenderas att radera"
" gamla inställningsfiler (som ``*.dpkg-{new,old}``-filer i ``/etc``) från"
" systemet."

