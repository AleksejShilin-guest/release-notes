# Translation of old-stuff.po to Brazilian Portuguese language.
# Translation of Debian release notes to Brazilian Portuguese.
# Copyright (C) 2009-2021 Debian Brazilian Portuguese l10n team
# <debian-l10n-portuguese@lists.debian.org>
# This file is distributed under the same license as the Debian release
# notes.
#
# Translators: Felipe Augusto van de Wiel <faw@debian.org>, -2009.
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2015.
# Adriano Rafael Gomes <adrianorg@debian.org>, 2017-2019.
# Revisors: Chanely Marques <chanelym@gmail.com>, 2011.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2013-2015.
# Tassia Camoes Araujo <tassia@debian.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Release Notes\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2024-04-22 20:43+0200\n"
"PO-Revision-Date: 2021-03-20 23:33-0400\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@debian.org>\n"
"Language: pt_BR\n"
"Language-Team: l10n Brazilian Portuguese <debian-l10n-"
"portuguese@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../old-stuff.rst:4
msgid "Managing your |OLDRELEASENAME| system before the upgrade"
msgstr "Gerenciando seu sistema |OLDRELEASENAME| antes da atualização"

#: ../old-stuff.rst:6
msgid ""
"This appendix contains information on how to make sure you can install or"
" upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|."
msgstr ""
"Este apêndice contém informações sobre como assegurar-se de que você "
"consegue instalar ou atualizar pacotes da |OLDRELEASENAME| antes de "
"atualizar para a |RELEASENAME|."

#: ../old-stuff.rst:12
msgid "Upgrading your |OLDRELEASENAME| system"
msgstr "Atualizando seu sistema |OLDRELEASENAME|"

#: ../old-stuff.rst:14
#, fuzzy
msgid ""
"Basically this is no different from any other upgrade of |OLDRELEASENAME|"
" you've been doing. The only difference is that you first need to make "
"sure your package list still contains references to |OLDRELEASENAME| as "
"explained in `Checking your APT source-list files <#old-sources>`__."
msgstr ""
"Basicamente, isso não é diferente de qualquer outra atualização do "
"|OLDRELEASENAME| que você tenha feito. A única diferença é que você "
"precisa ter certeza de que sua lista de pacotes ainda contém referências "
"para o |OLDRELEASENAME| conforme explicado em `Checking your APT source-"
"list files <#old-sources>`__."

#: ../old-stuff.rst:19
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically "
"be upgraded to the latest |OLDRELEASENAME| point release."
msgstr ""
"Caso você atualize o seu sistema usando um espelho Debian, ele "
"automaticamente será atualizado para a última versão pontual do "
"|OLDRELEASENAME|."

#: ../old-stuff.rst:25
msgid "Checking your APT source-list files"
msgstr "Verificando seus arquivos source-list do APT"

#: ../old-stuff.rst:27
msgid ""
"If any of the lines in your APT source-list files (see :url-man-"
"stable:`sources.list(5)`) contain references to \"stable\", this is "
"effectively pointing to |RELEASENAME| already. This might not be what you"
" want if you are not yet ready for the upgrade. If you have already run "
"``apt update``, you can still get back without problems by following the "
"procedure below."
msgstr ""
"Se qualquer uma das linhas nos seus arquivos source-list do APT (veja "
":url-man-stable:`sources.list(5)`) contiver referências a \"stable\", "
"você já está efetivamente \"apontando\" para a |RELEASENAME|. Isso pode "
"não ser o que você quer caso você ainda não esteja pronto para a "
"atualização. Caso você já tenha executado ``apt update``, você ainda pode"
" voltar atrás sem problemas seguindo o procedimento abaixo."

#: ../old-stuff.rst:34
msgid ""
"If you have also already installed packages from |RELEASENAME|, there "
"probably is not much point in installing packages from |OLDRELEASENAME| "
"anymore. In that case you will have to decide for yourself whether you "
"want to continue or not. It is possible to downgrade packages, but that "
"is not covered here."
msgstr ""
"Caso você também já tenha instalado pacotes do |RELEASENAME|, "
"provavelmente não há razão para instalar pacotes do |OLDRELEASENAME|. "
"Neste caso, você terá que decidir por você mesmo se quer continuar ou "
"não. É possível rebaixar a versão dos pacotes (\"downgrade\"), mas isso "
"não é abordado neste documento."

#: ../old-stuff.rst:40
#, fuzzy
msgid ""
"As root, open the relevant APT source-list file (such as "
"``/etc/apt/sources.list``) with your favorite editor, and check all lines"
" beginning with"
msgstr ""
"Como root, abra o arquivo source-list do APT relevante (tal como "
"``/etc/apt/sources.list``) com seu editor favorito, e verifique todas as "
"linhas começando com "

#: ../old-stuff.rst:44
msgid "``deb http:``"
msgstr ""

#: ../old-stuff.rst:45
msgid "``deb https:``"
msgstr ""

#: ../old-stuff.rst:46
msgid "``deb tor+http:``"
msgstr ""

#: ../old-stuff.rst:47
msgid "``deb tor+https:``"
msgstr ""

#: ../old-stuff.rst:48
msgid "``URIs: http:``"
msgstr ""

#: ../old-stuff.rst:49
msgid "``URIs: https:``"
msgstr ""

#: ../old-stuff.rst:50
msgid "``URIs: tor+http:``"
msgstr ""

#: ../old-stuff.rst:51
msgid "``URIs: tor+https:``"
msgstr ""

#: ../old-stuff.rst:53
#, fuzzy
msgid ""
"for a reference to \"stable\". If you find any, change \"stable\" to "
"\"|OLDRELEASENAME|\"."
msgstr ""
"para determinar se existe uma referência a \"stable\". Caso você encontre"
" qualquer uma, altere de \"stable\" para |OLDRELEASENAME|."

#: ../old-stuff.rst:55
msgid ""
"If you have any lines starting with ``deb file:`` or ``URIs: file:``, you"
" will have to check for yourself if the location they refer to contains a"
" |OLDRELEASENAME| or |RELEASENAME| archive."
msgstr ""
"Caso você tenha linhas começando com ``deb file:`` ou ``URIs: file:``, "
"você mesmo terá que verificar por você mesmo se o local indicado contém "
"um repositório da |OLDRELEASENAME| ou da |RELEASENAME|."

#: ../old-stuff.rst:61
msgid ""
"Do not change any lines that begin with ``deb cdrom:`` or ``URIs: "
"cdrom:``. Doing so would invalidate the line and you would have to run "
"``apt-cdrom`` again. Do not be alarmed if a ``cdrom:`` source line refers"
" to \"unstable\". Although confusing, this is normal."
msgstr ""
"Não mude nenhuma linha que comece com ``deb cdrom:`` ou ``URIs: cdrom:``."
" Fazer isso invalidaria a linha e você teria que executar o ``apt-cdrom``"
" novamente. Não se preocupe se uma linha para uma fonte do tipo "
"``cdrom:`` apontar para \"unstable\". Embora confuso, isso é normal."

#: ../old-stuff.rst:67
msgid "If you've made any changes, save the file and execute"
msgstr "Caso você tenha feito quaisquer mudanças, salve o arquivo e execute"

#: ../old-stuff.rst:73
msgid "to refresh the package list."
msgstr "para atualizar a lista de pacotes."

#: ../old-stuff.rst:78
msgid "Performing the upgrade to latest |OLDRELEASENAME| release"
msgstr ""

#: ../old-stuff.rst:80
msgid ""
"To upgrade all packages to the state of the latest point release for "
"|OLDRELEASENAME|, do"
msgstr ""

#: ../old-stuff.rst:90
msgid "Removing obsolete configuration files"
msgstr "Removendo arquivos de configuração obsoletos"

#: ../old-stuff.rst:92
msgid ""
"Before upgrading your system to |RELEASENAME|, it is recommended to "
"remove old configuration files (such as ``*.dpkg-{new,old}`` files under "
"``/etc``) from the system."
msgstr ""
"Antes de atualizar o seu sistema para |RELEASENAME|, é recomendado "
"remover arquivos de configuração antigos (tais como arquivos "
"``*.dpkg-{new,old}`` em ``/etc``) do sistema."

